var $ = jQuery;

function glsPSMap_OnSelected_Handler(data) {
  $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][id]']").val(data.pclshopid);
  $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][name]']").val(data.name);
  $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][address]']").val(data.zipcode + " " + data.city + " " + data.address);
}

(function ($, Drupal, drupalSettings) {
  var glsMap;

  /**
   * Handles the GLS CsomagPont selector in checkout.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.glsCsomagpontSelected = {
    attach: function attach(context) {
      glsMap = new GLSPSMap();
      glsMap.init("HU", "pickup-gls-csomagpont-map-canvas", null, 1);
      google.maps.event.trigger(glsMap, "resize");
    }
  };
})(jQuery, Drupal, drupalSettings);