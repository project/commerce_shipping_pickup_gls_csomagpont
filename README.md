# Commerce Shipping Pickup GLS CsomagPont

Drupal 8/9/10 module to extend the [commerce_shipping_pickup_api](https://www.drupal.org/project/commerce_shipping_pickup_api) module with pickup service providers.

Implemented providers:
- [GLS CsomagPont (Hungary) - Csomagautomaták (Parcel machines)](https://csomag.hu)

A _Google Maps JavaScript API_ key is required for the map to function.
See https://developers.google.com/maps/documentation/javascript/get-api-key.
